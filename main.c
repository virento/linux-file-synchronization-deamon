#include <string.h>
#include <errno.h>
#include <syslog.h>
#include "filesyn.h"
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>


int SLEEP_TIME_S = 300;
volatile int isSyncing = 0;
volatile int syncAfter = 0;

//Signal handler
void handler (int signum) {
  syslog(LOG_INFO, "Signal from user, weaking deamon!\n");
  if (isSyncing) {
    syncAfter = 1;
  }
}

//Are str1 start with str2
int equelStart(char* str1, char* str2) {
  size_t len1 = strlen(str1);
  size_t len2 = strlen(str2);
  if (strncmp(str1, str2, len1) == 0) return 0;
  if (strncmp(str1, str2, len2) == 0) return 0;
  return 1;
}


int main (int argc, char** args) {
  if(argc < 3) {
    printf("Wrong arguments, use:(<source path> <target path>)\n");
    exit(EXIT_FAILURE);
  }

  char* spath = args[1]; //Source directory
  char* tpath = args[2]; //Target directory

  printf("Source path: %s\nTarget path: %s\n", spath, tpath);

  //512MB
  off_t fast_tr = 512 * 1024 * 1024;

  //Arguments parsing
  int recursion = 0;
  int c;
  while ((c = getopt(argc - 2, args + 2, "t:s:R")) != -1) {
    switch (c) {
      case 's':
        sscanf(optarg, "%ld", &fast_tr);
        fast_tr *= 1024 * 1024;
        break;
      case 't':
        sscanf(optarg, "%d", &SLEEP_TIME_S);
        break;
      case 'R':
        recursion = 1;
        break;
      default:
        printf("Wrong argument \"-%c\"\n", optopt);
        exit(EXIT_FAILURE);
    }
  }


  if (!isDirectory(spath)) {
    printf("Path \"%s\" is not a directory!\n", spath);
    exit(EXIT_FAILURE);
  }

  if (!isDirectory(tpath)) {
    printf("Path \"%s\" is not a directory\n", tpath);
    exit(EXIT_FAILURE);
  }

  if (recursion && !equelStart(tpath, spath)) {
    printf("One of the directory is in second one!\n");
    exit(EXIT_FAILURE);
  }


  pid_t pid, sid;

  pid = fork();
  if (pid < 0) {
    perror("Error");
    exit(EXIT_FAILURE);
  } else if (pid > 0) {
    exit(EXIT_SUCCESS);
  }

  openlog("deamon-KB", LOG_PID, LOG_DAEMON);

  umask(0);

  sid = setsid();
  if (sid < 0) {
    syslog(LOG_ERR, "Setsid() failed: %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);


  signal(SIGUSR1, handler);

  while(1) {
    isSyncing = 1;
    syslog(LOG_INFO, "Performing directory synchronization\n");
    filesync(spath, tpath, fast_tr, recursion);
    syslog(LOG_INFO, "Directory has been synchronized\n");
    isSyncing = 0;

    //If signal SIGUSR1 was sended while syncing
    if (syncAfter) {
      syncAfter = 0;
      continue;
    }
    sleep(SLEEP_TIME_S);
  }

  exit(EXIT_SUCCESS);
}
