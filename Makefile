OBJ = main.o filesyn.o
all: deamon
deamon: $(OBJ)
	gcc $(OBJ) -o deamon
$(OBJ): filesyn.h
.PHONY: clean
clean:
	-rm -f *.o deamon
