#include <sys/stat.h>

/**
  * Check if path is pointing to a directory
  * @s_path path
  * @return 1 if file, 0 otherwise
  */
int isDirectory (char* path);

/**
  * Check if path is pointing to a file
  * @path path
  * @return 1 if directory, 0 otherwise
  */
int isFile(char* path);

/**
  * Sync file between to folder
  * @s_path path to source directory
  * @t_path path to target directory
  * @fast_tr limit for using read/write.
  * If size of file is greater than fast_tr then
  * sendfile() will be used to copy the file
  * @recursion determine if directorys should by copied
  */
void filesync(char* s_path, char* t_path, off_t fast_tr, int recursion);

/**
  * Remove directory and it's contents
  * @path path to directory
  */
void remove_dir(char* path);

/**
  * Copy file and change access nad modify time of it
  * @s_path path to source file
  * @t_path path to target file (where file should be saved)
  * @s_ttr source file attributes
  * @fast_tr limit for using read/write.
  * If size of file is greater than fast_tr then
  * sendfile() will be used to copy the file
  */
void copy_file (char* s_path, char* t_path, struct stat *s_attr, off_t fast_tr);

/**
  * Add '/' char at the end of path if not exist
  * @path path to be checked
  */
void fixPath (char* path);
