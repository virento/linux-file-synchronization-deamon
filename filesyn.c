#include <unistd.h>
#include <sys/sendfile.h>
#include <sys/time.h>
#include <time.h>
#include <utime.h>
#include <stdio.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <dirent.h>
#include <string.h>
#include <syslog.h>

void fixPath (char* path) {
  int length = strlen(path);
  if (path[length - 1] != '/') {
    path[length] = '/';
    path[length + 1] = '\0';
  }
}

int isDirectory (char* path) {
  struct stat st;
  int s = lstat(path, &st);
  if(s != 0) return -1; //File does not exist
  return S_ISDIR(st.st_mode) != 0 ? 1 : 0;
}

int isFile(char* path) {
  struct stat st;
  int s = lstat(path, &st);
  if(s != 0) return -1; //File does not exist
  return S_ISREG(st.st_mode) != 0 ? 1 : 0;
}

void copy_file (char* s_path, char* t_path, struct stat *s_attr, off_t fast_tr) {
  int s_file = open(s_path, O_RDONLY);
  if (s_file == -1) {
    syslog(LOG_ERR, "Can't open file \"%s\": %s\n", s_path, strerror(errno));
    return;
  }

  int t_file = open(t_path, O_RDWR | O_CREAT | O_TRUNC, s_attr->st_mode);
  if (t_file == -1) {
    syslog(LOG_ERR, "Can't open file \"%s\": %s\n", t_path, strerror(errno));
    return;
  }

  off_t fileSize = s_attr->st_size;
  off_t fileOffset = 0;

  int readed = 0;
  if (fast_tr < fileSize) {
    //If file is bigger than fast_tr use sendfile
    while ((readed = sendfile(t_file, s_file, &fileOffset, fileSize)) > 0) {
       fileSize -= readed;
    }
    if (readed == -1) {
      syslog(LOG_ERR, "Error transfering file \"%s\": %s\n", s_path, strerror(errno));
      return;
    }
  } else {
    //Otherwise use read/write
    int chunk_size = s_attr->st_blksize;
    char buffer[chunk_size];
    while ((readed = read(s_file, buffer, chunk_size)) > 0) {
      write(t_file, buffer, readed);
    }
    if (readed == -1) {
      syslog(LOG_ERR, "Error trasfering file \"%s\": %s\n", s_path, strerror(errno));
      return;
    }
  }

  close(s_file);
  close(t_file);

  syslog(LOG_ERR, "File has been sync: %s\n", s_path);

  struct utimbuf newtime;
  newtime.actime = s_attr->st_atime;
  newtime.modtime = s_attr->st_mtime;

  if (utime(t_path, &newtime) == -1) {
    syslog(LOG_ERR, "Error changing file time \"%s\": %s", t_path, strerror(errno));
    return;
  }
}

void remove_dir(char* path) {
  DIR* dir = opendir(path);
  if (dir == NULL) {
    syslog(LOG_ERR, "Error opening directory \"%s\": %s\n", path, strerror(errno));
    return;
  }

  char entry_path[PATH_MAX + 1];
  strncpy(entry_path, path, sizeof(entry_path));
  fixPath(entry_path);

  struct dirent* entry;
  struct stat attr;
  int path_len = strlen(entry_path);
  while ((entry = readdir(dir)) != NULL) {
    //Skip files: ".", ".."
    if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..")) continue;
    //Make full file path
    strncpy(entry_path + path_len, entry->d_name, sizeof(entry_path) - path_len);
    if (stat(entry_path, &attr) == -1) {
      syslog(LOG_ERR, "Error: \"%s\", %s\n",path, strerror(errno));
      continue;
    }
    if (S_ISDIR(attr.st_mode)) {
      remove_dir(entry_path);
    } else {
      if (S_ISREG(attr.st_mode)) {
        if (unlink(entry_path) == -1) {
          syslog(LOG_ERR, "Error removing file \"%s\": %s\n", entry_path, strerror(errno));
        } else {
          syslog(LOG_INFO, "File removed: \"%s\"\n", entry_path);
        }
      }
    }
  }
  closedir(dir);
  if (rmdir(path) == -1) {
    syslog(LOG_ERR, "Removing directory error \"%s\": %s\n", path, strerror(errno));
  } else {
    syslog(LOG_INFO, "Directory removed: \"%s\"\n", path);
  }
  return;
}

void filesync (char* s_path, char* t_path, off_t fast_tr, int recursion) {
  //Open directorys stream
  DIR* s_dir = opendir(s_path);
  if (s_dir == NULL) {
    syslog(LOG_ERR, "Error opening directory \"%s\": %s\n", s_path, strerror(errno));
    return;
  }

  DIR* t_dir = opendir(t_path);
  if (t_dir == NULL) {
      if(recursion && errno == ENOENT && !mkdir(t_path, 0777)
        && (t_dir = opendir(t_path)) != NULL) {
          syslog(LOG_INFO, "Directory created: %s\n", t_path);
      } else {
        syslog(LOG_ERR, "Error opening directory \"%s\": %s\n",s_path, strerror(errno));
        return;
      }
  }

  char entry_path_t[PATH_MAX + 1];
  char entry_path_s[PATH_MAX + 1];
  strncpy(entry_path_s, s_path, sizeof(entry_path_s));
  strncpy(entry_path_t, t_path, sizeof(entry_path_t));
  fixPath(entry_path_s);
  fixPath(entry_path_t);

  struct dirent* entry;
  struct stat t_attr, s_attr;
  int s_path_len = strlen(entry_path_s);
  int t_path_len = strlen(entry_path_t);
  while ((entry = readdir(s_dir)) != NULL) {
    //Skip files: ".", ".."
    if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..")) continue;
    //Make full files path
    strncpy(entry_path_s + s_path_len, entry->d_name, sizeof(entry_path_s) - s_path_len);
    strncpy(entry_path_t + t_path_len, entry->d_name, sizeof(entry_path_t) - t_path_len);
    //int t_stat_st = stat(entry_path_t, &t_attr);

    if (stat(entry_path_s, &s_attr) == -1) {
      syslog(LOG_ERR, "Error-stat: \"%s\": %s\n",entry_path_s, strerror(errno));
      continue;
    }

    int t_stat_st = stat(entry_path_t, &t_attr);

    if (t_stat_st != 0) { //Target file not exist
      if (S_ISDIR(s_attr.st_mode) && recursion)
        filesync(entry_path_s, entry_path_t, fast_tr, recursion);
      else if (S_ISREG(s_attr.st_mode))
        copy_file(entry_path_s, entry_path_t, &s_attr, fast_tr);
    } else if (S_ISREG(s_attr.st_mode)) { //Source file is regular file
      if (s_attr.st_mtime != t_attr.st_mtime) {
        copy_file(entry_path_s, entry_path_t, &s_attr, fast_tr);
      }
    } else if (S_ISDIR(s_attr.st_mode)) { //Source file is directory
      if(recursion) {
        filesync(entry_path_s, entry_path_t, fast_tr, recursion);
      }
    }
  }
  closedir(s_dir);

  //Remove file from target path that does not exist in source path
  while ((entry = readdir(t_dir)) != NULL) {
    //Skip files: ".", ".."
    if(!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..")) continue;
    //Make full files path
    strncpy(entry_path_s + s_path_len, entry->d_name, sizeof(entry_path_s) - s_path_len);
    strncpy(entry_path_t + t_path_len, entry->d_name, sizeof(entry_path_t) - t_path_len);

    int t_stat = stat(entry_path_t, &t_attr);
    int s_stat = stat(entry_path_s, &s_attr);
    if (s_stat == -1) {
      if (S_ISREG(t_attr.st_mode)) {
        if (unlink(entry_path_t) == -1) {
          syslog(LOG_ERR, "Error removing file \"%s\": %s\n", entry_path_t, strerror(errno));
        } else {
          syslog(LOG_INFO, "File removed: %s\n", entry_path_t);
        }
      } else if (recursion && S_ISDIR(t_attr.st_mode)) {
        remove_dir(entry_path_t);
      }
    }
  }
  closedir(t_dir);
}
